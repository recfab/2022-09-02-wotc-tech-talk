# 2022-09-02 WotC Tech Talk

This repo contains the slides corresponding to the Wizards' internal tech talk given on Friday, September 2, 2022.

The topic of the talk is _To Microservices and Back Again_, originally presented by Alexandra Noonan of Segment at _QCon 2020_ in London.

## Viewing the slideshow

Clone the repo and visit `dest/index.html` in a browser.

## Building the slideshow locally

You will need the following tools in your `PATH`

- [Pandoc](https://pandoc.org)
- [PlantUML](https://plantuml.com/)

Run make at the root of the repo.

```shell
$ make
```
