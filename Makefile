FORMAT := revealjs
SLIDE_LEVEL := 2

IMAGES := dist/images/*.png
IMAGES_DIR := $(dir $(IMAGES))

dist/index.html: index.md $(IMAGES)
	pandoc -t $(FORMAT) \
		--slide-level $(SLIDE_LEVEL) \
		--citeproc --bibliography references.yaml \
		--standalone \
		$< -o $@

dist/images/%.png: %.puml
	plantuml $^ -o $(IMAGES_DIR)
