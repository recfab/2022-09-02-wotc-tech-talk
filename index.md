---
title: Friday Tech Talk
subtitle: To Microservices and Back Again
author: Yael Namen (<yael.namen@wizards.com>)
date: September 2, 2022
---
# Let's watch together {.r-fit-text}

<iframe width="560" height="315" src="https://www.youtube.com/embed/hIFeaeZ9_AI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[@noonan2020]

# What does Segment do? {.r-fit-text}

Collect events from customers, and replicate those events to destinations owned by the same customer

# Types of Errors

## Retryable Errors

We can send this request to the destination again later, unchanged

(e.g. `429 Too Many Requests`)

## Non-retryable

The destination will ever accept this request

(e.g. `400 Bad Request`)

# Problem

Head of line blocking
: The event at the head of the queue cannot be processed and is holding up the rest of the queue.

# Architecture
## 2013: Original Monolith

![](images/arch-2013.png)

## 2014: Microservices

![](images/arch-2014.png)

## 2018: New Monolith

![](images/arch-2018.png)

# It's trade-offs all the way down

## Operational overhead

A monolith is much simpler to operate, due to fewer moving parts.

## Improved Modularity

Microservices force you to define you boundaries.

## Environmental Isolation

Microservices are isolated from each other at the process level and communicate through APIs

## Out-of-box visibility

Observability tools identify the component the event was sourced from.

# Solutions

## Centrifuge

A single deployable that functions as a queue per customer, per destination, without the attendant operational complexity.

## Traffic Recorder

A testing utility that allows the developer to record API sessions and play them back later.

Allows tests to be written against the recorded sessions, removing the need to run them against live environments, thereby speeding up the execution time of the test suite.

# Takeaway

None of these architectures solved the fundamental _head-of-line blocking_ error: it just reduced the blast radius

# References

::: {#refs}
:::
